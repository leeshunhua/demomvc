package com.simon.authentication.controller;

import com.simon.authentication.service.*;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class authenticationController {

	@RequestMapping("doLogin")
	public ModelAndView doLogin(HttpServletRequest request, HttpServletResponse response) {
		
		authenticationService auth = new authenticationService();
		ModelAndView mv = new ModelAndView();
		
		String strEmail = request.getParameter("email");
		String strPassword = request.getParameter("password");
		
		if(auth.validate(strEmail, strPassword)) {
			mv.setViewName("dashboard");
		}else {
			mv.setViewName("login");
			mv.addObject("message", "Invalid user/password. Please try again.");
			mv.addObject("email", request.getParameter("email"));
		}
		
		return mv;
	}
	
//	@RequestMapping("add1")
//	public ModelAndView add1(@RequestParam("t1") int i, @RequestParam("t2") int j) {
//		//System.out.println("I'm here");
//		
//		authenticationService auth = new authenticationService();
//		
//		
//		//int i = Integer.parseInt(request.getParameter("t1"));
//		//int j = Integer.parseInt(request.getParameter("t2"));
//		int k = auth.validate(i, j);
//		
//		ModelAndView mv = new ModelAndView();
//		mv.setViewName("login.jsp");
//		mv.addObject("result", k);
//		
//		return mv;
//	}
	
}
