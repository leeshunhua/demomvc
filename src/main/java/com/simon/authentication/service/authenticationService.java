package com.simon.authentication.service;

import java.util.LinkedHashMap;

import com.simon.authentication.model.UserDAO;
import com.simon.authentication.model.dbConnectionModel;

public class authenticationService {

	public boolean validate(String i, String j) {
		
		boolean isUser = false;
		
		dbConnectionModel dbcm = new dbConnectionModel();
		
		UserDAO user = new UserDAO();
		
		LinkedHashMap<String, String> userHashMap = new LinkedHashMap<String, String>();
		
		userHashMap = dbcm.selectUser(i, j);
		
		user.setIduser(userHashMap.get("iduser")); 
		user.setPassword(userHashMap.get("password"));
		user.setName(userHashMap.get("name"));
		user.setContact(userHashMap.get("contact"));
		user.setGender(userHashMap.get("gender"));
		user.setInterest(userHashMap.get("interest"));
		
		if(i.equalsIgnoreCase(user.getIduser()) && j.equals(user.getPassword())) {
			isUser = true;
		}
		
		return isUser;
	}
	
}
